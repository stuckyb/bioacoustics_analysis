


# Instructions for annotating audio files

We use [Raven Lite](http://ravensoundsoftware.com/software/raven-lite/) to view and annotate audio files.  Raven Lite is free to use, but you will need to [request a free license key](https://store.birds.cornell.edu/ProductDetails.asp?ProductCode=RAVENLITE) to fully enable it.  You will need to [download and install Raven Lite](http://ravensoundsoftware.com/raven-lite-downloads/) on your personal computer.

There is no user manual specific to Raven Lite, but there is a very detailed [user manual for Raven Pro](http://ravensoundsoftware.com/wp-content/uploads/2017/11/Raven14UsersManual.pdf), which is identical to Raven Lite in many ways.  The most relevant pages are 141-146, which discuss working with audio selections, and pages 179-182, which discuss working with annotations.


## Basic instructions

1. Launch Raven Lite and open an audio file.
2. You can use the toolbar buttons to manipulate sound playback, but it is also useful to know that `<spacebar>` is a shortcut to start and pause playback.
3. Create a new audio selection by clicking and dragging with your mouse on the oscillogram/waveform or spectrogram view.  After initially creating the selection, you can use the selection's resize handles to fine-tune the selection.  Try to make the selection boundaries as precise as possible so that the selection accurately captures the target sound.
4. Once you are happy with the selection, press `<enter>`.  This will "commit" (save) the selection and automatically bring up the annotation window.
5. Type the annotation (see "Annotation Format", below) and again press `<enter>`.  Return to step 3 to create a new selection and annotation.
6. To save your work, choose `Save Selection Table` from the `File` menu and choose a name for the annotations file.  _The format of the file name is very important!_  Name the file as `AUDIO FILE NAME` + `-` + `YOUR INITIALS` + `.txt`.  E.g., `recorder_1_01-BJS.txt`.
7. Once you have saved the selection table, you can close Raven Lite and reload the selection table the next time you want to work with the audio file.


## Annotation format

We record up to 3 pieces of information for each annotation: the type of sound, the taxa involved, and (optionally) the annotator's confidence in the taxonomic identification(s).  These pieces of information should be separated by a semicolon, so the basic format is `[SOUND TYPE];[TAXA];[UNCERTAINTY]`.  Each piece of information should be recorded as follows:
1. Record the type of sound with a single character:
    * **s** : single individual
    * **c** : single-species chorus
    * **m** : multi-species chorus (_all species are from the target taxonomic group_).
    * **o** : multi-species chorus (_some species are **not** from the target taxonomic group_).
2. Record the taxa as a comma-separated list.  E.g., `Megatibicen dorsatus,Neotibicen pruinosus`.
3. If you are uncertain about the annotation, indicate this using the appropriate code: If you are uncertain whether the sound is from the target group (e.g., whether it is even a frog sound), record the uncertainty as `u`; if you are uncertain about the taxonomic identification, record the uncertainty as `t`.  If you are uncertain about both, record the uncertainty as `ut` or `tu`.

So, an example of a complete annotation might be: `s;Megatibicen dorsatus,Neotibicen pruinosus;t`.  If you are confident about the annotation, you do not need to record any uncertainty information.

Before starting, I recommend choosing short abbreviations for frequently encountered taxa so that there is no need to type the full names over and over again.  For example, the last annotation above might be converted to `s;md,np;t`.


## Creating a metadata file

We'll need to know about any taxonomic name abbreviations you used so that we can work with your annotations later.  To record this information, please create a plain-text file that maps abbreviations to full taxonomic names as follows:
```
md: Megatibicen dorsatus
np: Neotibicen pruinosus
```

