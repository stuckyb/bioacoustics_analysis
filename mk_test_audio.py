#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import soundfile as sf
import numpy as np
import math
import sys


if len(sys.argv) != 2:
    exit('\nPlease provide an output file name.\n')

time_s = 2.0
samp_rate = 44100

freqs = [440]
# A-major chord.
freqs = [440, 554.4, 659.3, 880]
# Add a duplicate chord an octave higher.
freqs += [freq * 2 for freq in freqs[1:]]

amps = [0.8]
#amps = [0.8, 0.24]

noise = 0.0
#noise = 0.2

dc_offset = 0.0

# Calculate total number of samples.
samp_cnt = int(samp_rate * time_s)

samps = np.empty((samp_cnt, len(freqs)))

for cnt, freq in enumerate(freqs):
    # Generate the audio samples.
    samps[:,cnt] = np.linspace(0.0, time_s, num=samp_cnt, endpoint=False)
    samps[:,cnt] = np.sin(samps[:,cnt] * math.pi * 2.0 * freq)

# Mix the audio down to a single time series.
mixed = np.mean(samps, axis=1)

# Adjust the signal volume.
samprange = math.ceil(samp_cnt / len(amps))
for cnt, amp in enumerate(amps):
    mixed[cnt * samprange : (cnt + 1) * samprange] *= amp

# Add noise.
if noise > 0.0:
    noise = np.random.uniform(-1.0, 1.0, samp_cnt) * 0.2
    mixed = (mixed + noise) / 2

mixed += dc_offset

sf.write(sys.argv[1], mixed, samplerate=samp_rate, format='WAV')

